"use client"

import { usePageTitle } from '@/context/PageTitleContext'
import { ChevronRight } from 'lucide-react'
import React from 'react'

type Props = {}

export default function PageTitle({}: Props) {

  const {pageTitle} = usePageTitle()

  return (
    <div className='flex gap-1 w-full h-[70px] justify-start items-center pl-6 border-b'>
      { pageTitle?.first && <p className='flex gap-1 items-center text-xl font-medium'>{pageTitle.first}</p> }
      { pageTitle?.second && <p className='flex gap-1 items-center text-xl font-medium text-zinc-300'><ChevronRight />{pageTitle.second}</p> }
      { pageTitle?.third && <p className='flex gap-1 items-center text-xl font-medium text-zinc-300'><ChevronRight />{pageTitle.third}</p> }
    </div>
  )
}