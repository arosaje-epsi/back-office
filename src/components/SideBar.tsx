"use client"

import { useAuth } from '@/context/AuthContext';
import React, { ReactElement } from 'react'
import { Avatar, AvatarFallback, AvatarImage } from './ui/avatar';
import { toast } from './ui/use-toast';
import axios from 'axios';
import { Flower2, HandHelping, Home, Router, UsersRound } from 'lucide-react';
import { usePathname, useRouter } from 'next/navigation';
import logo from '../../public/Icon_Vert.svg'
import Image from 'next/image';
import { Separator } from "@/components/ui/separator"


type Props = {}
type MenuLink = {
  name: string;
  icon: ReactElement
  url: string;
}

export default function SideBar({}: Props) {
  const {userData, setUserData} = useAuth();
  const router = useRouter();
  const pathname = usePathname();

  const menuLinks:MenuLink[] = [
    {
      name: 'Accueil',
      icon: <Home size={20} />,
      url: '/'
    },
    {
      name: 'Utilisateurs',
      icon: <UsersRound size={20} />,
      url: '/users'
    },
    {
      name: 'Gardes',
      icon: <Flower2 size={20} />,
      url: '/products'
    },
    {
      name: 'Conseils',
      icon: <HandHelping size={20} />,
      url: '/orders'
    }
  ] 
  
  const logout = async () => {
    try {
      const res = await axios.get(process.env.NEXT_PUBLIC_AUTH_API_URL + '/logout', {
        withCredentials: true
      })
      setUserData(null);
      router.refresh();
    } catch (error) {
      toast({
        title: 'Error',
        description: 'Something went wrong',
        variant: 'destructive',
        duration: 5000,
      })
    }
  }

  if(userData){
    return (
      <div className='flex flex-col justify-between items-center w-72 h-screen p-4 border-r'>
        
        <div className='flex flex-col w-full h-fit gap-4'>
          <Image src={logo} alt='logo' width={40} />
          <Separator />

          <div className='flex flex-col w-full h-fit gap-2'>
            {
              menuLinks.map((link, index) => (
                <div
                key={index}
                onClick={() => router.push(link.url)}
                className={`flex items-center gap-2 p-2 w-full rounded-md cursor-pointer transition-all hover:bg-zinc-200 hover:bg-opacity-60 ${(link.url !== '/' && pathname.startsWith(link.url) || (pathname === link.url)) ? 'text-green-500' : 'text-zinc-800'}`}>
                  {link.icon}
                  <p className='font-medium'>{link.name}</p>
                </div>
              ))
            }
          </div>
        </div>

        <div className='flex flex-col justify-center w-full gap-4'>
          <div className='flex items-center p-2 gap-2 border rounded-full'>
            <Avatar>
              <AvatarImage src={userData.avatar} />
              <AvatarFallback>{userData.firstname[0] + userData.lastname[0]}</AvatarFallback>
            </Avatar>
            <p className='font-semibold text-zinc-800'>{userData.firstname}</p>
          </div>
          <p
          onClick={() => logout()}
          className='font-semibold text-zinc-800 hover:text-red-500 text-center cursor-pointer transition-all'>
            Déconnexion
          </p>
        </div>

      </div>
    )
  }
}