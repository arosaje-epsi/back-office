"use client"

import React, { Dispatch, useContext, useState } from 'react'

type Props = {
  children: React.ReactNode,
  title: any
}

interface PageTitleContextType {
  pageTitle: pageTitle | null;
  setPageTitle: Dispatch<React.SetStateAction<pageTitle | null>>
}

const PageTitleContext = React.createContext<PageTitleContextType | undefined>(undefined);

export const usePageTitle = () => {
  const { pageTitle, setPageTitle } = useContext(PageTitleContext) as PageTitleContextType;
  return { pageTitle, setPageTitle }
}

export default function PageTitleProvider({children, title}: Props) {
  const [pageTitle, setPageTitle] = useState(title)

  return (
    <PageTitleContext.Provider value={{pageTitle, setPageTitle}}>
      {children}
    </PageTitleContext.Provider>
  )
}