"use client"

import React, { Dispatch, useContext, useState } from 'react'

type Props = {
  children: React.ReactNode,
  user: any
}

interface AuthContextType {
  userData: loggedUser | null;
  setUserData: Dispatch<React.SetStateAction<loggedUser | null>>
}

const AuthContext = React.createContext<AuthContextType | undefined>(undefined);

export const useAuth = () => {
  const { userData, setUserData } = useContext(AuthContext) as AuthContextType;
  return {userData, setUserData}
}

export default function AuthProvider({children, user}: Props) {
  const [userData, setUserData] = useState(user)

  return (
    <AuthContext.Provider value={{userData, setUserData}}>
      {children}
    </AuthContext.Provider>
  )
}