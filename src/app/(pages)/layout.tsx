"use client"

import PageTitle from "@/components/PageTitle";
import PageTitleProvider from "@/context/PageTitleContext";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });



export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  const pageTitle:pageTitle = {}


  return (
    <div className="flex flex-col w-full h-screen overflow-hidden">
      <PageTitleProvider title={pageTitle}>
        <PageTitle />
        {children}
      </PageTitleProvider>
    </div>

  );
}
