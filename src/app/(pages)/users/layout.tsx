"use client"

import PageTitle from "@/components/PageTitle";
import PageTitleProvider, { usePageTitle } from "@/context/PageTitleContext";

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  const {setPageTitle} = usePageTitle();

  setPageTitle({first: 'Users'})

  return (
    <>
      {children}
    </>
  );
}
