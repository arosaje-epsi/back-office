import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Toaster } from "@/components/ui/toaster"
import AuthProvider from "@/context/AuthContext";
import axios from "axios";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";
import SideBar from "@/components/SideBar";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Arosaje Admin",
  description: "Admin dashboard",
};


export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  let user:loggedUser | undefined = undefined;
  const token = cookies().get('jwt')?.value || undefined

  try {

    const res = await axios.post(
      process.env.NEXT_PUBLIC_AUTH_API_URL + '/checkToken',
      {
        jwt: token
      },
      {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true
      }
    );

    if(res.data.data.user.role !== 'admin') throw new Error('Not admin')

    user = res.data.data.user

  } catch (error:any) {
    
  }

  return (
    <html lang="fr">
      <body className={inter.className}>
        <AuthProvider user={user}>
          <div className="flex w-full h-screen overflow-hidden bg-zinc-100">
            <SideBar/>
            <div className="w-full">
              
              {children}
            </div>
          </div>
        </AuthProvider>
        <Toaster />
      </body>
    </html>
  );
}
