"use client"
 
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
 
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { toast } from "@/components/ui/use-toast"

import Image from 'next/image'
import Logo from '../../../public/Logo_VertNoir.svg'
import { useEffect, useState } from "react"
import { Loader2 } from "lucide-react"
import { useRouter } from "next/navigation"
import axios from "axios"
import { useAuth } from "@/context/AuthContext"
 
const FormSchema = z.object({
  email: z.string().min(7, { message: " "}),
  password: z.string().min(3, { message: " "}),
})

type Props = {}

export default function Login({}: Props) {

  const {userData, setUserData} = useAuth();
  const router = useRouter();
  const [formSent, setFormSent] = useState(false);

  useEffect(() => {
    if(userData) router.push('/') 
  }, [userData, router]);

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  })
 
  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    setFormSent(true);
    
    try {
      const res = await axios.post(
        process.env.NEXT_PUBLIC_AUTH_API_URL + '/login/admin',
        data,
        {
          withCredentials: true
        }
      );

      setUserData(res.data.data.user)
      router.push('/');
      router.refresh();

    } catch (err) {
      loginError();
    }
  }

  const loginError = () => {
  
    setFormSent(false);
    toast({
      title: "Erreur",
      description: "Email ou mot de passe incorrect",
      variant: "destructive",
      duration: 5000,
    })
  
  }
 
  return (
    <div className="flex w-full h-screen overflow-hidden ">
      <div className="flex flex-col gap-9 w-full h-screen justify-center items-center">
        <Image src={Logo} width={130} height={130} alt="logo" />
        <div className="flex w-96 h-fit bg-white p-4 rounded-2xl border">
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="w-full space-y-6">
            <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Adresse email</FormLabel>
                    <FormControl>
                      <Input placeholder="john@doe.com" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Mot de passe</FormLabel>
                    <FormControl>
                      <Input type="password" placeholder="password" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <Button type="submit" className="w-full" disabled={formSent}>
                {formSent ? <Loader2 className="mr-2 h-4 w-4 animate-spin" /> : ''}
                Se connecter
              </Button>
            </form>
          </Form>
        </div>
      </div>
      <div className="w-full h-screen bg-[url('https://images.pexels.com/photos/1302883/pexels-photo-1302883.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1')] bg-cover bg-center">
        <div className="w-full h-screen bg-green-500 bg-opacity-30"></div>
      </div>
    </div>
  )
}