"use client"

import { useAuth } from '@/context/AuthContext'
import { Flower2, HelpingHand, HomeIcon, UsersRound } from 'lucide-react'
import { useRouter } from 'next/navigation'
import React, { ReactElement } from 'react'
type Props = {}

type shortLink = {
  name: string;
  url: string;
  icon: ReactElement
}

export default function Home({}: Props) {
  const {userData} = useAuth()
  const router = useRouter();
  const shortLinks: shortLink[] = [
    {
      name: 'Utilisateurs',
      url: '/users',
      icon: <UsersRound size={40} strokeWidth='1.25' />
    },
    {
      name: 'Gardes',
      url: '/guards',
      icon: <Flower2 size={40} strokeWidth='1.25' />
    },
    {
      name: 'Conseils',
      url: '/advices',
      icon: <HelpingHand size={40} strokeWidth='1.25' />
    }
  ]
  return (
    <div className='flex flex-col gap-8 justify-center items-center w-full h-screen overflow-hidden'>
      <h2 className='text-2xl font-semibold '>Bonjour, {userData?.firstname} 🌿</h2>
      <div className='flex gap-8'>
        {
          shortLinks.map(link => {return(
            <div onClick={() => router.push(link.url)} key={link.name} className='flex flex-col gap-2 w-36 aspect-square border justify-center items-center rounded-xl cursor-pointer hover:border-green-500 hover:text-green-500 transition-all'>
              {link.icon}
              <p className='font-medium'>{link.name}</p>
            </div>
          )})
        }
      </div>
    </div>
  )
}