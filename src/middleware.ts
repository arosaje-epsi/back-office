import axios from "axios";
import { redirect } from "next/dist/server/api-utils";
import { NextRequest, NextResponse } from "next/server";

export async function middleware(req: NextRequest) {
	if (!req.cookies.has("jwt") && !req.nextUrl.pathname.startsWith("/login")) {
		return NextResponse.redirect(new URL("/login", req.url));
	} else if (!req.nextUrl.pathname.startsWith("/login)")) {
		const token = req.cookies.get("jwt")?.value;

		const res = await fetch(
			process.env.NEXT_PUBLIC_AUTH_API_URL + "/checkToken",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					jwt: token,
				}),
				credentials: "include",
			}
		).then((res) => res.json());

    if(res.data && res.data.user){
      return NextResponse.next()
    }else{
      let res = NextResponse.redirect(new URL("/login", req.url))
      res.cookies.delete("jwt")
      return res
    }

	} else {
    return NextResponse.next();
  }
}

export const config = {
	matcher: [
		/*
		 * Match all request paths except for the ones starting with:
		 * - api (API routes)
		 * - _next/static (static files)
		 * - _next/image (image optimization files)
		 * - favicon.ico (favicon file)
		 */
		{
			source: "/((?!api|login|_next/static|_next/image|favicon.ico).*)",
			missing: [
				{ type: "header", key: "next-router-prefetch" },
				{ type: "header", key: "purpose", value: "prefetch" },
			],
		},
	],
};
