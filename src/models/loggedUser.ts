type loggedUser = {
    id: string,
    email: string,
    firstname: string,
    lastname: string,
    role: string,
    avatar: string,
    createdAt: Date
}